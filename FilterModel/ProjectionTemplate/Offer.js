const ProjectionTemplate = {
    Offer: {
        default: ()=> {
            return {
                'address': {
                $concat: [
                  '$land.road',
                  { $cond: [{ $and: [{ $ne: ['$land.road', ''] }, { $ne: ['$land.address', ''] }] }, ' ', ''] },
                  '$land.address',
                  { $cond: [{ $and: [{ $ne: [{ $concat: ['$land.road', '$land.address'] }, ''] }, { $ne: [{ $concat: ['$land.zipCode', '$land.city'] }, ''] }] }, ', ', ''] },
                  '$land.zipCode',
                  { $cond: [{ $and: [{ $ne: ['$land.zipCode', ''] }, { $ne: ['$land.city', ''] }] }, ' ', ''] },
                  '$land.city'
                ]
              }
            }
        }
    }
}