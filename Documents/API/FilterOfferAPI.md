# Filter Offer API

## GET

### INPUT

- offset : UInteger

- perpage : UInteger

- sort : String 
        
        - Data will sorted by this vaule of offer

- dir : String 
        
        - desc 5 4 3 2 1
        - asc 1 2 3 4 5

- search  (SearchText) : String that will search on these field

        - land
                - road : String
                - zipCode : String
                - address : String
                - city : String
                - addition : String

        - agent
                - fname : String
                - mname : String
                - lname : String
                - company.name : String
                
        - creatorName : String
        - assigneeName : String

- state  (state name) : String

        - incomplete
        - unverified
        - phaseOne
        - phaseTwo
        - rejected
        - interested

- expire  (expire status) : String

        - none
        - default
        - nearly
        - expired

- filter  (FilterData) : JSON Object as encode string with PARAMETER:

        - assignee : match ID
                - { from all users }
        - creator : match ID
                - { from all users }
        - contactAt : range Date *select as Year sent as date range*

        - agent  *(select object agent but set 4 of this if have value)*
                - fname : match String
                - mname : match String
                - lname : match String
                - company.name : match String

        - land
                - zipCode : range String
                        - { 00000 10000 20000 .... 80000 90000 }
                        - { 09999 19999 29999 .... 89999 99999 }
                - city : match String
                        - { all cities }
                - area : range Float
                        -  {'< 50' 50 100 150 .... 1950 2000 '> 2000' }
                - usableArea : range Float 
                        - {'< 50' 50 100 150 .... 1950 2000 '> 2000' }

### Sub FilterData Type

- match *( type 'string' - ignore case sensitive and accent sensitive )*
       
        {
                t : { id date string number }
                eq : <equal value> (optional)
                gt : <greater than> (optional)
                lt : <less than> (optional)
                gte : <greater than or equal> (optional)
                lte : <less than  or equal> (optional)
        }

### Sample

        {host}/filter?
        offset=0&
        perpage=15&
        sort=createdAt&
        dir=desc&
        expire=expired& - {none, default, nearly, expired} **
        state=unverified&
        s=willow&
        f={
                'creator' : { 't' : 'id', 'eq' : {id of user} },
                'assignee' : { 't' : 'id', 'eq' : {id of user} },

                'land.zipCode' : { 't' : 'string', 'gte' : '20000', 'lte' : '29999' },
                'land.city' : { 't' : 'string', 'eq':'Hamburg' },
                'land.area' : { 't' : 'number', 'gte' : '50', 'lte' : '100' },
                'land.usableArea' : { 't' : 'number', 'gte' : '50', 'lte' : '100' },

                'agent.fname' : { 't' : 'string', 'eq' : 'Name' },
                'agent.mname' : { 't' : 'string', 'eq' : 'Of' },
                'agent.lname' : { 't' : 'string', 'eq' : 'Agent' },
                'agent.company.name' : { 't' : 'string', 'eq' : 'Company' },

                'contactAt' : 
                { 't' : 'date', 'gte' : '{start date of year 2016}', 'lte' : '{end date of year 2016}' } 

                'state.expiredAt' :
                 { 't' : 'date', 'lt' : '{date now}' } - expired **

                'state.expiredAt' : 
                { 't' : 'date', 'gte' : '{date now}' ,'lt' : '{date now + 2days}' } - nearly **

                'state.expiredAt' : 
                { 't' : 'date', 'gte' : '{date now + 2days}' } - default **
        }

- **f is JSON enacode string*
- ***selected one method to filter 'expire status'*
  1. *use enum name 'expire'*
  2. *use as filter*